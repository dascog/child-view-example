import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent-block',
  templateUrl: './parent-block.component.html',
  styleUrls: ['./parent-block.component.css']
})
export class ParentBlockComponent implements OnInit {

  showChild:boolean = false;
  textInputText:string = "";

  constructor() { }

  ngOnInit(): void {
  }

  onShowChild():void {
    this.showChild = !this.showChild;
  }

  onTextInputEvent(event:any):void {
    this.textInputText = event.target.value;
    console.log(this.textInputText);
  }
}

