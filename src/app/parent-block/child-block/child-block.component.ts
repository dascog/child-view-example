import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-block',
  templateUrl: './child-block.component.html',
  styleUrls: ['./child-block.component.css']
})
export class ChildBlockComponent implements OnInit {

  @Input ()
  childInputText!: string;

  constructor() { }

  ngOnInit(): void {
  }

}
